import logging

import varstore


def nop():                  # do NOTHING!
    pass


def add():                  # add the 2 top stack items
    varstore.stack_push(varstore.stack_pop_ret() + varstore.stack_pop_ret())


def sub():                  # subtract the 2 top stack items
    varstore.stack_push(varstore.stack_pop_ret() - varstore.stack_pop_ret())


def div():                  # divide the 2 top stack items
    varstore.stack_push(int(varstore.stack_pop_ret() / varstore.stack_pop_ret()))


def mul():                  # multiply the 2 top stack items
    varstore.stack_push(varstore.stack_pop_ret() * varstore.stack_pop_ret())


def read():                 # push an inputted number onto the stack
    varstore.stack_push(int(input("input: ").strip()))


def echo():                 # print the top stack item as a number
    print(varstore.stack_top())


def echo_char():            # print the top stack item as a character
    print(chr(varstore.stack_top()), end='')


def inc():                  # increment the top stack item
    varstore.stack_push(varstore.stack_pop_ret() + 1)


def dec():                  # decrement the top stack item
    varstore.stack_push(varstore.stack_pop_ret() - 1)


def dupe():                 # duplicate the top stack item
    varstore.stack_push(varstore.stack_top())


def pop():                  # pop an item from the stack
    varstore.stack_pop()


def push(value: int):       # push {value} onto the stack
    varstore.stack_push(value)


def mem_get(addr: int):     # push the value of memory address {addr} onto the stack
    varstore.stack_push(varstore.memory[addr])


def mem_set(addr: int):     # set memory address {addr} to the top stack value
    varstore.memory[addr] = varstore.stack_pop()


def goto(inst_num: int):    # go to subroutine
    return f"GOTO{inst_num}"


def ret():                  # return from subroutine
    return "RET"


def end():                  # end program
    import sys
    sys.exit()


def mem_dump():             # dump memory to file "memdump.bin"
    logging.getLogger("memory_dump").info("Writing memory dump to 'memdump.bin' in binary mode")
    with open("memdump.bin", "wb") as f:
        f.write(varstore.memory.data)


instructions = {
    0x00: nop,
    0x01: add,
    0x02: sub,
    0x03: div,
    0x04: mul,
    0x05: read,
    0x06: echo,
    0x07: echo_char,
    0x08: inc,
    0x09: dec,
    0x0A: dupe,
    0x0B: pop,
    0x0C: push,
    0x0D: mem_get,
    0x0E: mem_set,
    0x0F: goto,
    0x10: ret,
    0x11: end,
    0x12: mem_dump
}
