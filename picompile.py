from colorama import Fore, Back
import logging
import argparse
import numpy as np
import instructions
from inspect import signature
from ver import VERSION
from main import MAGIC


def compile_instructs(file: list, filename: str = "<unknown>"):
    logger = logging.getLogger("instruction_compilation")
    instructs = []
    instruct_map = {}
    logger.debug("Creating instruction map")
    for b, inst in instructions.instructions.items():
        logger.debug(f"Mapping instruction {inst.__name__} to {hex(b)}")
        instruct_map[inst.__name__] = b
    for i, line in enumerate(file):
        line = line.strip()
        logger.debug(f"Interpreting line '{line}' (file {filename} line {i})")
        if line.startswith("//") or line.startswith("#"):
            logger.debug("Ignoring comment")
            continue
        args = line.split()
        inst = instruct_map[args[0]]
        output = [inst]
        args = args[1:]
        for idx, arg in enumerate(args):
            logger.debug(f"Interpreting argument {arg} (arg {arg} index {idx + 1} file {filename} line {i})")
            if isinstance(arg, str):
                if arg.startswith("0x"):
                    logger.debug(f"Converting argument {arg} to hex")
                    arg = int(arg)
                else:
                    if len(arg) > 1:
                        logger.error(f"String arguments can't be longer than 1! If you want to input hex, prefix it "
                                     f"with '0x'. (arg {arg} index {idx + 1} file {filename} line {i})")
                    else:
                        logger.debug(f"Converting argument {arg} to ascii value")
                        arg = ord(arg)
            logger.debug(f"Appending argument {int(arg)}")
            output.append(int(arg))
        instructs += output
    return instructs


def main():
    print(Fore.RESET + Back.RESET, end="")

    # Command-line args
    parser = argparse.ArgumentParser(description="PiCode PiCompile for PiC " + VERSION)

    parser.add_argument("--verbose", "-v", action="store_true", help="Enable verbose logging")

    parser.add_argument("--nologo", "-n", action="store_true", help="Don't show the version info")

    parser.add_argument('in_file',
                        help="The PiCode file to be compiled",
                        type=argparse.FileType('r'))

    parser.add_argument('out_file',
                        default='prog.bin',
                        help="The PiC binary output file",
                        type=argparse.FileType('wb'))

    args = parser.parse_args()

    # Logging
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format=f'{Fore.RESET}[{Fore.YELLOW}%(asctime)s{Fore.RESET} / {Fore.RED}%(levelname)s{Fore.RESET}]{Fore.GREEN} %(name)s{Fore.RESET}: %(message)s'
    )

    logger = logging.getLogger("picompile")

    # Logo
    if not args.nologo:
        print(Fore.YELLOW + "PiCode PiCompile for PiC " + VERSION + Fore.RESET + "\n")

    logger.debug(f"Reading file {args.in_file.name}")
    in_file = args.in_file.readlines()
    out_binary = args.out_file
    logger.debug(f"Compiling {len(in_file)} lines")
    output = compile_instructs(in_file, args.in_file.name)
    logger.debug(f"Adding magic bytes ({[hex(b) for b in MAGIC]})")
    output = bytearray(MAGIC + output)
    logger.debug(f"Writing output binary {out_binary.name}")
    out_binary.write(output)


if __name__ == '__main__':
    main()
