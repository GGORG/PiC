from colorama import Fore, Back
import logging
import argparse
import varstore
import numpy as np
import instructions
from inspect import signature
from ver import VERSION

MAGIC = [0xFF, 0x00, 0xFF]


def allocate_memory(size: int):
    logger = logging.getLogger("mem_allocation")

    if size < 100:
        logger.warning(f"Not enough memory for reserved data! You need to allocate {100 - size} more")

    if size < 200:
        logger.warning(f"Not enough memory for the full stack! You need to allocate {200 - size} more")

    varstore.init_memory(size)


def prepare(args):
    logger = logging.getLogger("preparation")

    logger.debug("Preparing environment...")

    logger.debug("Initializing variable store...")
    varstore.init()

    logger.debug("Allocating and testing memory...")
    allocate_memory(args.memory)


def execute(instruction: int, args: list, idx: int, instructs: list, filename: str = "<unknown>"):
    logger = logging.getLogger("execution")
    logger.debug(f"Executing instruction {hex(instruction)} with args {args} (file {filename} position {idx})...")
    try:
        ret = instructions.instructions[instruction](*args)
        if ret is not None:
            if ret.startswith("GOTO"):
                addr = int(ret[4:])
                logger.debug(f"Entering subroutine {addr}")
                pos = 0
                sr_instructs = instructs[addr:]
                for i, (b, args) in enumerate(sr_instructs):
                    ret = execute(b, args, pos, instructs, filename)
                    if ret is not None and ret == "RET":
                        logger.debug("Leaving subroutine")
                        break
                    pos += 1
                    pos += len(args)
        else:
            return True

    except Exception:
        logger.exception(
            f"Error while executing instruction {hex(instruction)} (file {filename} position {idx})!")
        return False


def execute_program(instructs: list, filename: str = "<unknown>"):
    logger = logging.getLogger("program_execution")
    try:
        pos = 0
        for i, (b, args) in enumerate(instructs):
            if b == 0x10:
                logger.error(f"Not in a subroutine!")
            execute(b, args, pos, instructs, filename)
            pos += 1
            pos += len(args)
    except KeyboardInterrupt:
        logger.info("CTRL-C detected, stopping!")


def interpret_instructions(binary: list, filename: str = "<unknown>"):
    logger = logging.getLogger("instruction_interpretation")
    args_left = 0
    instruction = (0x00, [])
    instructs = []
    for i, b in enumerate(binary):
        if args_left > 0:
            logger.debug(f"Interpreting argument {hex(b)} of instruction {hex(instruction[0])} (file {filename} position {i})")
            args_left -= 1
            instruction = (instruction[0], instruction[1] + [b])
            if args_left == 0:
                instructs.append(instruction)
        else:
            logger.debug(f"Interpreting instruction {hex(b)} (file {filename} position {i})")
            if b not in list(instructions.instructions.keys()):
                logger.error(f"Instruction {hex(b)} invalid! (file {filename} position {i})")
            else:
                logger.debug("Instruction valid!")

            inst = instructions.instructions[b]
            logger.debug(f"Getting signature of function {inst.__name__}")
            sig = signature(inst)
            args_left = len(sig.parameters)
            instruction = (b, [])
            if args_left == 0:
                instructs.append(instruction)
    return instructs


def check_magic(binary, filename: str = "<unknown>"):
    logger = logging.getLogger("magic_check")
    logger.debug(f"Checking magic bytes ({[hex(b) for b in MAGIC]}) for file {filename}")
    if list(binary)[:len(MAGIC)] == MAGIC:
        logger.debug("Magic bytes match!")
        return True
    else:
        logger.warning(f"Magic bytes ({[hex(b) for b in MAGIC]}) don't match the file {filename}! It may be a file of a different format")
        return False


def main():
    print(Fore.RESET + Back.RESET, end="")

    # Command-line args
    parser = argparse.ArgumentParser(description="PiC " + VERSION)

    parser.add_argument("--verbose", "-v", action="store_true", help="Enable verbose logging")

    parser.add_argument("--nologo", "-n", action="store_true", help="Don't show the version info")

    parser.add_argument('--memory', '-m',
                        default=524288,
                        help="Set the allocated memory table size. Defaults to 10000",
                        type=int)

    parser.add_argument('file',
                        help="The binary file to be executed",
                        type=argparse.FileType('rb'))

    args = parser.parse_args()

    # Logging
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        format=f'{Fore.RESET}[{Fore.YELLOW}%(asctime)s{Fore.RESET} / {Fore.RED}%(levelname)s{Fore.RESET}]{Fore.GREEN} %(name)s{Fore.RESET}: %(message)s'
    )

    logger = logging.getLogger("pic")

    # Logo
    if not args.nologo:
        print(Fore.YELLOW + "PiC " + VERSION + Fore.RESET + "\n")

    # Preparation
    prepare(args)

    logger.debug("Ready, starting execution.")

    binary = args.file.read()
    filename = args.file.name
    check_magic(binary, filename)
    instructs = interpret_instructions(binary[len(MAGIC):], filename)
    execute_program(instructs, filename)


if __name__ == '__main__':
    main()
