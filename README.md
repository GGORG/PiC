# PiC

PiC is a code execution environment which runs binary files written in the PiC binary language

## Memory

PiC has 2 types of memory. They are the short-term stack (stored in memory) and the long-term memory.
The memory is divided into 3 parts: 

| Address     | Description                       |
|-------------|:---------------------------------:|
| 0 - 100     | Reserved memory (unused for now)  |
| 101 - 200   | Stack                             |
| 201 +       | Free memory                       |

## Instructions

The list of instructions is growing, you can see all of them in the [instructions.py file](instructions.py). 

## Arguments

You can supply arguments to instructions by just adding them after the instruction. 1 byte is 1 argument.
You can see the arguments that you need to provide in the [instructions.py file](instructions.py).

## Magic

Every PiC binary file should begin with a series of magic bytes (`0xFF` `0x00` `0xFF`) defined at the top of the [main.py file](main.py). 

## Sample programs

 - Adder
    
    Adds 2 numbers together
    ```
    FF 00 FF 05 05 01 06
    ```
   
 - Hello, world!
    
    The classic hello world program
    ```
    FF 00 FF 0C 48 07 0B 0C 65 07 0B 0C 6C 07 0B 0C
    6C 07 0B 0C 6F 07 0B 0C 2C 07 0B 0C 20 07 0B 0C
    77 07 0B 0C 6F 07 0B 0C 72 07 0B 0C 6C 07 0B 0C
    64 07 0B 0C 21 07 0B 0A
    ```