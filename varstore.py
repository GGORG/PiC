import logging
import numpy as np

memory: np.ndarray
stack_pointer: int


def stack_push(value: int):
    global stack_pointer
    logger = logging.getLogger("stack")
    stack_pointer += 1
    if stack_pointer > 100:
        stack_pointer -= 100
    idx = 100 + stack_pointer
    try:
        memory[idx] = value
        logger.debug(f"Written stack index {stack_pointer} to memory address {idx} with value {hex(value)} ({value})")
    except IndexError:
        logger.error(f"Not enough memory! Can't write stack index {stack_pointer} to memory address {idx}")


def stack_pop():
    global stack_pointer
    logger = logging.getLogger("stack")
    idx = 100 + stack_pointer
    try:
        oldval = memory[idx]
        memory[idx] = 0x00
        logger.debug(f"Popped stack index {stack_pointer} from memory address {idx} with value {hex(oldval)} ({oldval})")
    except IndexError:
        logger.error(f"Not enough memory! Can't pop stack index {stack_pointer} from memory address {idx}")
    stack_pointer -= 1
    if stack_pointer < 0:
        stack_pointer += 100


def stack_top() -> int:
    logger = logging.getLogger("stack")
    idx = 100 + stack_pointer
    try:
        value = memory[idx]
        logger.debug(f"Read stack index {stack_pointer} from memory address {idx} with value {hex(value)} ({value})")
        return value
    except IndexError:
        logger.error(f"Not enough memory! Can't read stack index {stack_pointer} from memory address {idx}")


def stack_pop_ret() -> int:
    ret = stack_top()
    stack_pop()
    return ret


def init():
    global stack_pointer
    logger = logging.getLogger("var_store")
    logger.debug("Initializing variable store...")
    stack_pointer = 0


def init_memory(size: int):
    global memory
    logger = logging.getLogger("var_store")
    logger.debug(f"Initializing memory array of size {size}")
    memory = np.array([0 for _ in range(size)], dtype=np.byte)
